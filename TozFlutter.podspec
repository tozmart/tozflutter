Pod::Spec.new do |s|
  s.name                  = 'TozFlutter'
  s.version               = '0.0.5'
  s.summary               = 'Flutter module'
  s.description           = 'Flutter module - TozFlutter'
  s.homepage              = 'https://gitlab.com/tozmart/tozflutter'
  s.license               = { :type => 'MIT' }
  s.author                = { 'Tozmart Team' => 'www.tozmart.com' }
  s.source                = { :git => 'https://gitlab.com/tozmart/tozflutter.git', :tag => s.version.to_s } 
  s.platform              = :ios, '9.0'
  s.swift_version         = "5.0"
  s.pod_target_xcconfig   = {'VALID_ARCHS' => 'armv7 arm64 x86_64' }
  s.vendored_frameworks   = 'Flutter.xcframework'
end
